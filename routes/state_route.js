var mysql   = require('mysql');

var state_dal = require('../model/state_dal');
var express = require('express');
var router = express.Router();

/* DATABASE CONFIGURATION */

exports.GetAll = function(callback){
    var qry = "SELECT * from state;"
    connection.query(qry, function(err, result){
        callback(err, result);
    });
}

router.get('/create', function(req, res) {
    res.render('stateFormCreate.ejs');
});

router.get('/state_insert', function(req, res){
    state_dal.Insert(req.query.statename, function(err, result){
        var response = {};
        if(err) {
            response.message = err.message;
        }
        else {
            response.message = 'Success!';
        }
        res.json(response);
    });
});


module.exports = router;