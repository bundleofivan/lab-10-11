var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'CS355', subtitle: 'Lab 8'});
});
*/

router.get('/', function(req, res, next) {
  var data = {
    title : 'Express'
  }
  if(req.session.account === undefined) {
    res.render('index', data);
  }
  else {
    data.fname = req.session.account.fname;
    res.render('index', data);
  }
});

/* Get Template Example. */
router.get('/templateLink', function(req, res, next) {
  res.render('templateexample.ejs', {title: 'cs355'});

});

router.get('/authenticate', function(req, res) {
  userDal.GetByEmail(req.query.email, req.query.password, function (err, account) {
    if (err) {
      res.send(err);
    }
    else if (account == null) {
      res.send("Account not found.");
    }
    else {
      req.session.account = account;
      res.send(account);
    }
  });
});

router.get('/login', function(req, res) {
  res.render('authentication/login.ejs');
});

router.get('/logout', function(req, res) {
  req.session.destroy( function(err) {
    res.render('authentication/logout.ejs');
  });
});

module.exports = router;
